import { Pool } from 'pg'
import dotenv from 'dotenv'

dotenv.config()

const host: string | undefined = process.env.DB_HOST
const port: number | undefined = Number(process.env.DB_PORT)
const user: string | undefined = process.env.DB_USER
const password: string | undefined = process.env.DB_PASS
const database: string | undefined = process.env.DB_NAME

const pool = new Pool({
    host: host,
    port: port,
    user: user,
    password: password,
    database: database
})

export default pool
