function generateSuccessResponse(statusCode: number, data?: any) {
    return {
        status: {
          code: statusCode,
          message: 'success'
        },
        data: data || undefined
    }
}


function generateErrorResponse(error: any) {
    return {
      error: error.error
    }
}

export { generateErrorResponse, generateSuccessResponse }
