import express, { Router, Request, Response } from 'express'
import orderService from '../services/OrderService'
import { generateErrorResponse, generateSuccessResponse } from '../utilities/ResponseHandler'
import CustomError from '../utilities/CustomError'

const router: Router = express.Router()

router.get('/v1/orders', async(req: Request, res: Response) => {
    try {
        const response = await orderService.getOrders()

        res.status(200).json(generateSuccessResponse(200, response))
    } catch (err) {
        if (err instanceof CustomError) {
            const errorResponse = generateErrorResponse(err)
            res.status(err.statusCode).json(errorResponse)
        } else {
            res.status(500).json(generateErrorResponse('Internal server error'))
        }
    }
})

router.get('/v1/order/:id', async(req: Request, res: Response) => {
    try {
        const response = await orderService.getOrder(req.params as any)

        res.status(200).json(generateSuccessResponse(200, response))
    } catch (err) {
        if (err instanceof CustomError) {
            const errorResponse = generateErrorResponse(err)
            res.status(err.statusCode).json(errorResponse)
        } else {
            res.status(500).json(generateErrorResponse('Internal server error'))
        }
    }
})

router.post('/v1/order', async(req: Request, res: Response) => {
    try {
        await orderService.addOrder(req.body)

        res.status(200).json(generateSuccessResponse(200))
    } catch (err) {
        if (err instanceof CustomError) {
            const errorResponse = generateErrorResponse(err)
            res.status(err.statusCode).json(errorResponse)
        } else {
            res.status(500).json(generateErrorResponse('Internal server error'))
        }
    }
})

router.put('/v1/order', async(req: Request, res: Response) => {
    try {
        const response = await orderService.updateOrder(req.body)

        res.status(200).json(generateSuccessResponse(200, response))
    } catch (err) {
        if (err instanceof CustomError) {
            const errorResponse = generateErrorResponse(err)
            res.status(err.statusCode).json(errorResponse)
        } else {
            res.status(500).json(generateErrorResponse('Internal server error'))
        }
    }
})

router.delete('/v1/orders', async(req: Request, res: Response) => {
    try {
        await orderService.deleteAllOrder()

        res.status(200).json(generateSuccessResponse(200))
    } catch (err) {
        if (err instanceof CustomError) {
            const errorResponse = generateErrorResponse(err)
            res.status(err.statusCode).json(errorResponse)
        } else {
            res.status(500).json(generateErrorResponse('Internal server error'))
        }
    }
})

router.delete('/v1/order/:id', async(req: Request, res: Response) => {
    try {
        await orderService.deleteOrder(req.params as any)

        res.status(200).json(generateSuccessResponse(200))
    } catch (err) {
        if (err instanceof CustomError) {
            const errorResponse = generateErrorResponse(err)
            res.status(err.statusCode).json(errorResponse)
        } else {
            res.status(500).json(generateErrorResponse('Internal server error'))
        }
    }
})

export default router
