import express, { Express } from 'express'
import orderController from './routes/OrderController'
import dotenv from 'dotenv'

dotenv.config()

const app: Express = express()

app.use(express.json())

const port = 3000

app.use('/order', orderController)

app.listen(port, () => {
    console.log(`Server listening at ${port}`)
})
