export default interface AddOrderRequest {
    name: string,
    price: number
}
