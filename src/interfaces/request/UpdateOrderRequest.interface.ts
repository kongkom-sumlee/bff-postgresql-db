export default interface UpdateOrderRequest {
    id: number
    name: string,
    price: number
}
