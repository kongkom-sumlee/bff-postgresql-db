import AddOrderRequest from '../interfaces/request/AddOrderRequest.interface'
import DeleteOrderRequest from '../interfaces/request/DeleteOrderRequest.interface'
import GetOrderRequest from '../interfaces/request/GetOrderRequest.interface'
import UpdateOrderRequest from '../interfaces/request/UpdateOrderRequest.interface'
import DB from '../utilities/DB.config'
import helper from './OrderServiceHelper'
import CustomError from '../utilities/CustomError'

class OrderService {
    public handleResponse(response: any) {
        switch (true) {
            case !response:
                throw new CustomError(400, 'data not found')
        }

        return response
    }

    public async getOrders() {
        let data: any = await DB.query(`SELECT id, name, price FROM public."order" ORDER BY id ASC`)

        data = helper.mapOrder(data.rows)
        data = this.handleResponse(data)

        return data
    }

    public async getOrder(getOrderRequest: GetOrderRequest) {
        const query = `SELECT id, name, price FROM public."order" WHERE public."order"."id" = $1`
        const params = [ getOrderRequest.id ]
        let data: any = await DB.query(query, params)

        data = helper.mapOrder(data.rows)
        data = this.handleResponse(data[0])

        return data
    }

    public async addOrder(addOrderRequest: AddOrderRequest) {
        const query = `INSERT INTO public."order" (id, name, price) VALUES ($1, $2, $3)`
        const orders = await this.getOrders()
        const ordersId = helper.mapId(orders)
        const emptyOrdersId = helper.findEmptyId(ordersId)
        const params = [emptyOrdersId, addOrderRequest.name, addOrderRequest.price]

        await DB.query(query, params)
    }

    public async updateOrder(updateOrderRequest: UpdateOrderRequest) {
        const query = `UPDATE public."order" SET name = $2, price = $3 WHERE public."order"."id" = $1 RETURNING id, name, price`
        const params = [ updateOrderRequest.id, updateOrderRequest.name, updateOrderRequest.price ]

        let data: any = await DB.query(query, params)

        data = helper.mapOrder(data.rows)
        data = this.handleResponse(data[0])

        return data
    }

    public async deleteAllOrder() {
        await DB.query(`TRUNCATE public."order"`)
    }

    public async deleteOrder(deleteOrderRequest: DeleteOrderRequest) {
        const query = `DELETE FROM public."order" WHERE public."order"."id" = $1`
        const params = [ deleteOrderRequest.id ]

        await DB.query(query, params)
    }
}

export default new OrderService()
