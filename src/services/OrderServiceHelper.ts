class OrderServiceHelper {
    public findEmptyId(id: any[]): number {
        let nextId: number = 1

        id.forEach((currentId: any) => currentId === nextId ? nextId++ : nextId )

        return nextId
    }

    public mapId(items: any[]): number[] {
        const itemId: number[] = items.map((item) => Number(item.id))

        return itemId
    }

    public mapOrder(datas: any) {
        const mapedData = datas.map((data: any) => {
            return {
                id: Number(data.id),
                name: data.name,
                price: Number(data.price)
            }
        })

        return mapedData
    }
}

export default new OrderServiceHelper()
